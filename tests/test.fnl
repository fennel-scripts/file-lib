#!/usr/bin/env fennel
(local f (require :fil))
(local tcat table.concat)
(local tests {})
(local filename "tests/examplefile.txt")
(local firstline "this is a test")


(lambda pp [...]
  (let [fennel (require :fennel)]
	(print (fennel.view ...))))

(fn failed [] (print "" "[FAILED]"))
(fn passed [] (print "" "[PASSED]"))




(local dummy (f.dummy))
(when (assert (not (dummy:file?)) "dummy should not be an actual file") (passed))

(set dummy.name filename)
(let [file (f.create dummy)]
  (table.insert file.lines 1 firstline)
  (file:write!))

(local file (f.init dummy))
(when (assert (file:file?) "file should be an actual file") (passed))

(file:read!)
(when (assert (= (. file.lines 1) firstline) (.. "first line of file should be '" firstline "'")) (passed))

(f.enforce file)
