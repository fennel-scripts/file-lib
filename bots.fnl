#!/usr/bin/env fennel
(local bots {})
(local project (require :project))
(local projname project.name)
(local tcat table.concat)
(local fennel (require :fennel))
(print (fennel.searchModule :botcmd))
(local cmd (require :botcmd))
;;(require :lib.botsetup)
(require :botsetup)
(local rc (require :bots.rc))
(local dirs rc.dirs)

(fn pp [...] (print (fennel.view ...)))



(fn bots.init [self]
  (let [basedir (.. dirs.stow.src "/" project.name)
		mkdir cmd.mkdir]
	(mkdir {:dirs basedir})
	(each [_ v (pairs dirs.targets )]
	  (mkdir {:dirs (tcat [basedir "/" v])}))))
(set bots.setup bots.init)
(set bots.configure bots.init)

(fn bots.build [self]
  (let [install cmd.install
		name project.name
		bin dirs.targets.bin
		lib dirs.targets.lib
		stowdir dirs.stow.src
		licensedir dirs.targets.licenses]
;;: you can use this:
	;;(install {:src (tcat [name ".fnl"])	:tgt (tcat [stowdir "/" name "/" bin name])})
;;:OR this
	;;(install {:src (tcat ["main.fnl"])	:tgt (tcat [stowdir "/" name "/" bin name])})
;;: to "install" a file from /src/ to the users bin directory, removing the .fnl at the end

	;;and now the libraries, if you got any
	(install {:src "lib/fil.fnl"	:tgt (tcat [stowdir "/" name "/" lib	"fil.fnl"])})
	;;and the license
	(install {:src :LICENSE.org			:tgt (tcat [stowdir "/" name "/" licensedir  name ".org"])}))
  (print :done))


(fn bots.stow [self]
  (cmd.stow {:dir dirs.stow.src :target dirs.stow.tgt :action "stow" :pkgs [projname]})
  (print :done))

(fn bots.unstow [self]
  (cmd.stow {:dir dirs.stow.src :target dirs.stow.tgt :action "unstow" :pkgs [projname]})
  (print :done))

(fn bots.clean [self]
  (let [name project.name
		;;exec cmd.tprint
		exec cmd.exec
		stowdir dirs.stow.src]
	(self:unstow)
	(exec ["rm " "-r " stowdir "/" name] "")))


(fn bots.install [self]
  (doto self
	;;(: :init)
	(: :build)
	(: :stow)))

(set bots.uninstall bots.unstow)

bots
