#!/usr/bin/env fennel
(local f {})
(local tcat table.concat)

	  ;; (let [lines []]
	  ;; 	(with-open [file-in (io.open this.name)]
	  ;; 	  (each [line (file-in:lines)]
	  ;; 		(table.insert lines line)))
	  ;; 	(set this.lines lines))

(λ read! [this]
  (if (= this.name "-")
	  (icollect [k _ (io.lines) &into this.lines] k)
	  (icollect [k _ (io.lines this.name) &into this.lines] k)
	  )
  this
  )

(λ write! [this]
  (if (= this.name "-")
	  (each [lino line (ipairs this.lines)] (io.write (tcat "\n")))
	  (with-open [file-out (io.open this.name :w)]
		(print "writing...")
		(file-out:write (tcat this.lines "\n")))))

(lambda filter! [this func]
  "takes a file and a function
loops over the lines in the file and runs the function with <line> as the argument,
sets 'line' to the returned value, or the existing value of 'line' if false is returned"
  (assert (= (type func) "function") "filter must be given function(<line>)")
  (each [lino line (ipairs this.lines)]
	(tset this.lines lino (or (func line) line))
	(print (. this.lines lino))
	)
  this)

(λ f.enforce [{: name : read! : write! : lines : __is-file}]
  "will fail if *input* is not a *file*, its rudamentary but it **should** work"
  {: name : read! : write! : lines : __is-file})

(lambda f.is-file [file] (. (f.enforce file) :__is-file))

(lambda file? [this]
  (let [{:__is-file is-file} this]
	is-file))





(lambda f.new [{:name name :lines ?lines}]
  "takes a filename in and returns a *file*"
  (local file
		 {:name name 
		  :read! read!
		  :write! write!
		  :lines (or ?lines [])
		  :file? file?
		  :filter! filter!
		  :__is-file true}) file)

(lambda f.dummy []
  {:name "/dev/null"
   :lines []
   :__is-file false
   :file? file?})

(lambda f.create [file]
  ;; (lambda new-file [{: name : lines}]
  ;; 	{:name name
  ;; 	 :read! read!
  ;; 	 :write! write!
  ;; 	 :lines lines
  ;; 	 :file? file?
  ;; 	 :__is-file true})
  (collect [k v (pairs (f.new file)) &into file] k v)
  file)

(lambda f.init [{: name}]
  (let [file (f.new {: name})]
	(file:read!)
	file))
f
